<?php

if (!defined('IN_DISCUZ')) {
    exit('Access Denied');
}

$lang = array
(
    'rename_card_name' => '改名卡',
	'rename_card_desc' => '修改自己的用户名',
    'rename_card_input_new_username' => '请输入希望更换的新用户名',
    'rename_card_empty_input' => '请输入用户名',
    'rename_card_illegal_length' => '用户名长度不合法',
    'rename_card_username_exists' => '用户名已被占用',
    'rename_card_rename_succeed' => '改名成功'
);

?>
